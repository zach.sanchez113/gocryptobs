#!/usr/bin/python3
import argparse
import base64
import getpass
import hashlib
import io
import itertools
import json
import os
import struct
import sys

try:
    from Cryptodome.Cipher import AES
    from Cryptodome.Hash import SHA256
    from Cryptodome.Protocol.KDF import HKDF
except ImportError:
    from Crypto.Cipher import AES
    from Crypto.Hash import SHA256
    from Crypto.Protocol.KDF import HKDF

PLAINTEXT_ZERO = b"\x00" * 4096
CIPHERTEXT_ZERO = b"\x00" * (4096 + 32)


def decode_masterkey(masterkey):
    return bytes.fromhex(masterkey.replace("-", ""))


class GocryptfsConfig:
    def __init__(self, filename=None, basepath=None):
        if filename is None:
            basepath = os.path.abspath(basepath)
            while True:
                filename = os.path.join(basepath, "gocryptfs.conf")
                if os.path.isfile(filename):
                    break
                basepath = os.path.dirname(basepath)
                if basepath == "/":
                    raise RuntimeError("Failed to find gocryptfs.conf")

        with open(filename, "rb") as fp:
            self.config = json.load(fp)

        # We only support file systems created with gocryptfs version >= 1.3.
        # Older file systems did not use HKDF to derive separate keys yet.
        assert self.config["Version"] == 2
        assert "HKDF" in self.config["FeatureFlags"]

    def get_masterkey(self, password):
        scrypt = self.config["ScryptObject"]
        block = base64.b64decode(self.config["EncryptedKey"])

        scryptkey = hashlib.scrypt(
            password.encode("utf-8"),
            salt=base64.b64decode(scrypt["Salt"]),
            n=scrypt["N"],
            r=scrypt["R"],
            p=scrypt["P"],
            maxmem=0x7FFFFFFF,
            dklen=scrypt["KeyLen"],
        )

        key = HKDF(
            scryptkey, salt=b"", key_len=32, hashmod=SHA256, context=b"AES-GCM file content encryption"
        )

        assert len(block) > 32
        assert block != CIPHERTEXT_ZERO
        # Layout: [ NONCE | CIPHERTEXT (...) |  TAG  ]
        nonce, tag, ciphertext = block[:16], block[-16:], block[16:-16]
        aes = AES.new(key, AES.MODE_GCM, nonce=nonce)
        aes.update(struct.pack(">Q", 0))
        return aes.decrypt_and_verify(ciphertext, tag)

    @property
    def aessiv(self):
        return "AESSIV" in self.config["FeatureFlags"]



# TODO: Change these files to point to the correct place
config = GocryptfsConfig(
    filename="/home/zsanchez/cmiyc-2022/cmiyc-2022_street_3/1991whattimeisit/gocryptfs.conf",
    basepath="/home/zsanchez/cmiyc-2022/cmiyc-2022_street_3/1991whattimeisit/",
)

# TODO: Pick your favorites
passwords = [
    "password",
]

for password in passwords:
    try:
        masterkey = config.get_masterkey(password)
        print(f"The password appears to be {password}")
        sys.exit(0)

    # ValueError: MAC check failed
    except ValueError as e:
        ...


print("None of the passwords worked!")
sys.exit(1)
