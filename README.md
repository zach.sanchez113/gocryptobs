Code adapted from [gocryptfs-inspect](https://github.com/slackner/gocryptfs-inspect). This just checks if a password works, more specifically if it passes the MAC check.

To get started, I used Python 3.9 in a venv, then:

```bash
pip install pycryptodome
```

To use this, go to the TODOs at the bottom of the script, and follow them/change as needed.
